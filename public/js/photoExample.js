//Nos aseguramos que estén definidas
//algunas funciones básicas

$(document).ready(function(){

var oFoto = $('#foto');

window.URL = window.URL || window.webkitURL;
navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia ||
function() {
    alert('Su navegador no soporta navigator.getUserMedia().');
};

//Este objeto guardará algunos datos sobre la cámara
window.datosVideo = {
    'StreamVideo': null,
    'url': null
}

$('#foto').hide(function(){});

$('#botonIniciar').on('click', function(e) {

    //Pedimos al navegador que nos da acceso a 
    //algún dispositivo de video (la webcam)
    navigator.getUserMedia({
        'audio': false,
        'video': true
    }, function(streamVideo) {
        datosVideo.StreamVideo = streamVideo;
        datosVideo.url = window.URL.createObjectURL(streamVideo);
        $('#camara').attr('src', datosVideo.url);

    }, function() {
        alert('No fue posible obtener acceso a la cámara.');
    });

    $('#foto').hide('fast',function(){});
    $('#camara').show('slow',function(){});

});

/*

$('#botonDetener').on('click', function(e) {

    if (datosVideo.StreamVideo) {
        datosVideo.StreamVideo.stop();
        window.URL.revokeObjectURL(datosVideo.url);
    }

});
*/


$('#botonFoto').on('click', function(e) {
    var oCamara, oContexto, w, h;

    oCamara = $('#camara');
    
    w = oCamara.width();
    h = oCamara.height();
    oFoto.attr({
        'width': w,
        'height': h
    });
    oContexto = oFoto[0].getContext('2d');
    oContexto.drawImage(oCamara[0], 0, 0, w, h);

    $('#foto').show('slow',function(){});
    $('#camara').hide('fast',function(){});



     if (datosVideo.StreamVideo) {
        datosVideo.StreamVideo.stop();
        window.URL.revokeObjectURL(datosVideo.url);
    }

});

$('#Guardar').on('click',function(e){

    var photoCard = oFoto[0].toDataURL("image/png");
    var name = $('#name').val();
    var peopleType = $('#peopleType').val();
    var email = $('#email').val();
    var phoneHome = $('#phoneHome').val();
    var mobileHome = $('#mobileHome').val();

    if( name == '' || peopleType == '' || email == '' )
    {}
    else {

    var data = {name:name,photoCard:photoCard,peopleType:peopleType,email:email,phoneHome:phoneHome,mobileHome:mobileHome};


                 $.ajax({

                        type : "POST",
                        url : "/peoples",
                        data :data,
                        success : function(result){
                            setTimeout(function(){ window.location = '/visitInserted'; }, 30);

                        }
                    });
    }

});


$('#GuardarRes').on('click',function(e){

    var photoCard = oFoto[0].toDataURL("image/png");
    var name = $('#name').val();
    var houseId = $('#houseId').val();
    var email = $('#email').val();
    var phone1 = $('#phone1').val();
    var phone2 = $('#phone2').val();

    if( name == '' || houseId== '' || email == '' || phone1 == '')
    {}
    else {

    var data = {name:name,photoCard:photoCard,houseId:houseId,email:email,phone1:phone1,phone2:phone2};


                 $.ajax({

                        type : "POST",
                        url : "/residents",
                        data :data,
                        success : function(result){
                            setTimeout(function(){ window.location = '/residentInserted'; }, 30);

                        }
                    });
    }

});


});