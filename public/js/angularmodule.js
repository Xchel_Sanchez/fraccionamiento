
			var app = angular.module('Fraccionamiento',['ngRoute', 'ngCookies']);
			app.controller ('peopleController',function  ($scope , $http, $window, $cookies) {
				

				if ($cookies.user == '010101super'){
					$scope.userCookie = true;	
				}
				else{
					$scope.userCookie = false;

					if($cookies.user == 'Reader'){ $scope.onlyReadCookie = true; }

				}

				$scope.flag = true;

				$scope.newPhoto = function() {

					$scope.flag = true;

				}

				$scope.takePhoto = function() {

					$scope.flag = false;

				}
				

				
			});

			app.controller ('carController',function  ($scope , $http, $cookies) {
				
				if ($cookies.user == '010101super'){
					$scope.userCookie = true;	
				}
				else{
					$scope.userCookie = false;

					if($cookies.user == 'Reader'){ $scope.onlyReadCookie = true; }

				}
				
				$scope.car={};

				$scope.closePopUp = function(){

					 $window.close();
				}
			});


			app.controller ('residentController',function  ($scope , $http, $cookies) {

				if ($cookies.user == '010101super'){
					$scope.userCookie = true;	
				}
				else{
					$scope.userCookie = false;

					if($cookies.user == 'Reader'){ $scope.onlyReadCookie = true; }

				}


				$scope.flag = true;

				$scope.newPhoto = function() {

					$scope.flag = true;

				}

				$scope.takePhoto = function() {

					$scope.flag = false;

				}

				$scope.clusters;
				$scope.clusterId;

			

				$http.get('/clusters').
			    	success(function(data, status, headers, config) {
				      $scope.clusters = data.results;
				    }).
				    error(function(data, status, headers, config) {
				      // log error
				    });


				$scope.showHouse = function(){

					$http.post('/cluster/house',{clusterId:$scope.clusterId}).

			    	success(function(data, status, headers, config) {

			    		$scope.numberHouse = data.results;
				      
				    }).
				    error(function(data, status, headers, config) {
				      // log error
				    });

				}
			});


			app.controller ('visitController',function  ($scope , $http, $cookies) {
				
				if ($cookies.user == '010101super'){
					$scope.userCookie = true;	
				}
				else{
					$scope.userCookie = false;

					if($cookies.user == 'Reader'){ $scope.onlyReadCookie = true; }

				}

				$scope.clusterId;
				$scope.casaId;
				$scope.resident;
				$scope.peopleType;
				$scope.visitName;
				$scope.showCar = false;

				$scope.brand;
				$scope.color;
				$scope.model;
				$scope.placas;
				
				var socket = io();

				$scope.message = function(){
					var msg = {visitId: $scope.visitName, clusterId: $scope.clusterId, brand: $scope.brand, color:$scope.color, model:$scope.model, placas:$scope.placas }
					socket.emit('new visit', msg);	
				}
				

				$http.get('/clusters').
			    	success(function(data, status, headers, config) {
				      $scope.clusters = data.results;
				    }).
				    error(function(data, status, headers, config) {
				      // log error
				    });


				$scope.showHouse = function(){

					$http.post('/cluster/house',{clusterId:$scope.clusterId}).

			    	success(function(data, status, headers, config) {

			    		$scope.numberHouse = data.results;
				      
				    }).
				    error(function(data, status, headers, config) {
				      // log error
				    });

				}

				$scope.showResident = function(){


					$http.post('/house/resident',{houseId:$scope.casaId}).

			    	success(function(data, status, headers, config) {

			    		$scope.resident = data.results;
			    		
				      
				    }).
				    error(function(data, status, headers, config) {
				      // log error
				    });		

				}

				$scope.showPeople = function(){

					$http.post('/people/type',{type : $scope.peopleType}).

			    	success(function(data, status, headers, config) {

			    		$scope.peoples = data.results;
				      
				    }).
				    error(function(data, status, headers, config) {
				      // log error
				    });					

				}

			});

			app.controller ('residentSearchController',function  ($scope , $http, $cookies) {
				
				if ($cookies.user == '010101super'){
					$scope.userCookie = true;	
				}
				else{
					$scope.userCookie = false;

					if($cookies.user == 'Reader'){ $scope.onlyReadCookie = true; }

				}

				$scope.clusters;
				$scope.clusterId;
				$scope.houseId;
				
				

			

				$http.get('/clusters').
			    	success(function(data, status, headers, config) {
				      $scope.clusters = data.results;
				    }).
				    error(function(data, status, headers, config) {
				      // log error
				    });


				$scope.showHouse = function(){

					$http.post('/cluster/house',{clusterId:$scope.clusterId}).

			    	success(function(data, status, headers, config) {

			    		$scope.numberHouse = data.results;
				      
				    }).
				    error(function(data, status, headers, config) {
				      // log error
				    });

				}

				$scope.showHouseUpdate = function(){

					$http.post('/cluster/house',{clusterId:$scope.clusterIdUpdate}).

			    	success(function(data, status, headers, config) {

			    		$scope.numberHouseUpdate = data.results;
				      
				    }).
				    error(function(data, status, headers, config) {
				      // log error
				    });

				}
				$scope.showResident = function(){

					$http.post('/house/resident',{houseId:$scope.houseId}).

			    	success(function(data, status, headers, config) {

			    		$scope.residents = data.results;

			    		if(data){

			    			$http.post('/cluster/id',{clusterId:$scope.clusterId}).

					    	success(function(data, status, headers, config) {

					    		$scope.clusterNickname = data.nickname;
						      
						    })

						    $http.post('/house/id',{houseId:$scope.houseId}).

					    	success(function(data, status, headers, config) {

					    		$scope.houseNumber = data.number;
						      
						    })
						   }
				      
				    }).
				    error(function(data, status, headers, config) {
				      // log error
				    });		

				}


				$scope.showDetails = function (_id){


					$http.get('/resident/'+_id).

			    	success(function(data, status, headers, config) {
			    		
			    		$scope.residentDetails = data;
			    		
				    }).
				    error(function(data, status, headers, config) {
				      // log error
				    });				

				}

				$scope.deleteResident = function(_id) {

					$http.delete('/resident/'+_id).

			    	success(function(data, status, headers, config) {

			    		setTimeout(function(){ window.location = '/deleted'; }, 30);
			    		
				    }).
				    error(function(data, status, headers, config) {
				      // log error
				    });		

				}

				$scope.updateResident = function(_id){

					if($scope.residentDetails.name == undefined || $scope.residentDetails.phone1 == undefined || $scope.residentDetails.email == undefined)
						{}

					else{
						$http.put('/resident/'+_id, {name: $scope.residentDetails.name, phone1: $scope.residentDetails.phone1, phone2:$scope.residentDetails.phone2, email: $scope.residentDetails.email}).

						success(function(data, status, headers, config) {

							setTimeout(function(){ window.location = '/confirmation'; }, 30);
				    		
					    }).
					    error(function(data, status, headers, config) {
					      // log error
					    });
					}		

				}

				
			});

			app.controller ('visitSearchController',function  ($scope , $http, $cookies) {
				
				if ($cookies.user == '010101super'){
					$scope.userCookie = true;	
				}
				else{
					$scope.userCookie = false;

					if($cookies.user == 'Reader'){ $scope.onlyReadCookie = true; }

				}
						

				$scope.visitDate;
				$scope.hour;

				$scope.putHour = function(_id){

					$http.put('/visits/'+_id, {hour: $scope.hour}).

			    	success(function(data, status, headers, config) {
			    		
			    		setTimeout(function(){ window.location = '/confirmation'; }, 30);
			    		
				    }).
				    error(function(data, status, headers, config) {
				      // log error
				    });		

				}


				$scope.showVisit = function (){

					$http.post('/visit/date',{date : $scope.visitDate}).

			    	success(function(data, status, headers, config) {
			    		
			    		$scope.visits = data.results;

				    }).
				    error(function(data, status, headers, config) {
				      // log error
				    });		


				    $http.get('/visit/date').

			    	success(function(data, status, headers, config) {
			    		
			    		$scope.hour = data.hour;

				    }).
				    error(function(data, status, headers, config) {
				      // log error
				    });						
				}

				$scope.showDetails = function (_id){


					$http.get('/visits/'+_id).

			    	success(function(data, status, headers, config) {
			    		
			    		$scope.visitDetails = data;
			    		
				    }).
				    error(function(data, status, headers, config) {
				      // log error
				    });		

				    $http.get('/visits/'+_id+'/car').

			    	success(function(data, status, headers, config) {
			    		
			    		$scope.carDetails = data;
			    		
				    }).
				    error(function(data, status, headers, config) {
				      // log error
				    });				

				}

				$scope.deleteVisit = function(_id) {

					$http.delete('/visits/'+_id).

			    	success(function(data, status, headers, config) {

			    		setTimeout(function(){ window.location = '/deleted'; }, 30);
			    		
				    }).
				    error(function(data, status, headers, config) {
				      // log error
				    });		

				}



			});

			app.controller ('peopleSearchController',function  ($scope , $http, $cookies) {
				
				if ($cookies.user == '010101super'){
					$scope.userCookie = true;	
				}
				else{
					$scope.userCookie = false;

					if($cookies.user == 'Reader'){ $scope.onlyReadCookie = true; }

				}

				$scope.visitorId;
				$scope.flag = false;
				$scope.peopleType;				


				 $scope.showPeople = function(){

					$http.post('/people/type',{type : $scope.peopleType}).

			    	success(function(data, status, headers, config) {

			    		$scope.peoples = data.results;
				      
				    }).
				    error(function(data, status, headers, config) {
				      // log error
				    });					

				}

				$scope.showVisitor = function (){

					$http.post('/people/name',{id : $scope.visitorId}).

			    	success(function(data, status, headers, config) {
			    		
			    		$scope.visitors = data;
			    		$scope.flag = true;
			    		
				    }).
				    error(function(data, status, headers, config) {
				      // log error
				    });

				}


				$scope.showDetails = function (_id){


					$http.get('/people/'+_id).

			    	success(function(data, status, headers, config) {
			    		
			    		$scope.visitorDetails = data;

			    		$scope.name = data.name;
			    		$scope.peopleT = data.peopleType;
			    		$scope.mobilePhone = data.mobilePhone;
			    		$scope.phoneHome = data.phoneHome;
			    		$scope.email = data.email;
			    		
				    }).
				    error(function(data, status, headers, config) {
				      // log error
				    });		

				    $http.get('/people/'+_id+'/car').

			    	success(function(data, status, headers, config) {
			    		
			    		$scope.carDetails = data.results;
			    		
				    }).
				    error(function(data, status, headers, config) {
				      // log error
				    });				

				}

				$scope.deleteVisitor = function(_id) {

					$http.delete('/people/'+_id).

			    	success(function(data, status, headers, config) {

			    		setTimeout(function(){ window.location = '/deleted'; }, 30);
			    		
				    }).
				    error(function(data, status, headers, config) {
				      // log error
				    });		

				}

				$scope.updatePeople = function(_id){

					if($scope.name == '' || $scope.peopleT == '' || $scope.email == '')
						{}

					else{
						$http.put('/people/'+_id, {name: $scope.name, peopleType: $scope.peopleT, mobilePhone:$scope.mobilePhone, phoneHome: $scope.phoneHome, email: $scope.email}).

						success(function(data, status, headers, config) {

							setTimeout(function(){ window.location = '/confirmation'; }, 30);
				    		
					    }).
					    error(function(data, status, headers, config) {
					      // log error
					    });
					}		

				}

				
			});

			app.controller ('clusterController',function  ($scope , $http, $cookies) {
				
				if ($cookies.user == '010101super'){
					$scope.userCookie = true;	
				}
				else{
					$scope.userCookie = false;

					if($cookies.user == 'Reader'){ $scope.onlyReadCookie = true; }

				}
				
				$scope.flag = false;

				$http.get('/clusters').

			    	success(function(data, status, headers, config) {
			    		
			    		$scope.clusters = data.results;
			    		
				    }).
				    error(function(data, status, headers, config) {
				      // log error
				    });

				 $scope.deleteCluster = function (_id){

				 	$http.delete('/cluster/'+_id).

			    	success(function(data, status, headers, config) {
			    		
			    		setTimeout(function(){ window.location = '/deleted'; }, 30);
			    		
				    }).
				    error(function(data, status, headers, config) {
				      // log error
				    });

				 }

				 $scope.showDetails = function (_id){


					$http.get('/cluster/'+_id).

			    	success(function(data, status, headers, config) {
			    		
			    		$scope.clusterDetails = data;

			    		
				    }).
				    error(function(data, status, headers, config) {
				      // log error
				    });	
				 }

			});

			app.controller ('houseController',function  ($scope , $http, $cookies) {
				
				if ($cookies.user == '010101super'){
					$scope.userCookie = true;	
				}
				else{
					$scope.userCookie = false;

					if($cookies.user == 'Reader'){ $scope.onlyReadCookie = true; }

				}

				$scope.clusterId;

				$http.get('/clusters').

			    	success(function(data, status, headers, config) {
			    		
			    		$scope.clusters = data.results;
			    		
				    }).
				    error(function(data, status, headers, config) {
				      // log error
				    });


				$scope.showHouse = function(){

					$http.get('/cluster/'+ $scope.clusterId).

			    	success(function(data, status, headers, config) {

			    		$scope.clusterName = data;
				      
				    }).
				    error(function(data, status, headers, config) {
				      // log error
				    });

					$http.post('/cluster/house',{clusterId:$scope.clusterId}).

			    	success(function(data, status, headers, config) {

			    		$scope.numberHouse = data.results;
				      
				    }).
				    error(function(data, status, headers, config) {
				      // log error
				    });

				}


				$scope.deleteHouse = function (_id){

				 	$http.delete('/house/'+_id).

			    	success(function(data, status, headers, config) {
			    		
			    		setTimeout(function(){ window.location = '/deleted'; }, 30);
			    		
				    }).
				    error(function(data, status, headers, config) {
				      // log error
				    });

				 }


				 $scope.showDetails = function (_id){


					$http.get('/house/'+_id).

			    	success(function(data, status, headers, config) {
			    		
			    		$scope.houseDetails = data;

			    		
				    }).
				    error(function(data, status, headers, config) {
				      // log error
				    });	
				 }

			});

			app.controller('indexController' , function ($scope, $http, $cookies){

				if ($cookies.user == '010101super'){
					$scope.userCookie = true;	
				}
				else{
					$scope.userCookie = false;

					if($cookies.user == 'Reader'){ $scope.onlyReadCookie = true; }

				}


			});

			app.controller('userController' , function ($scope, $http, $cookies){

				if ($cookies.user == '010101super'){
					$scope.userCookie = true;	
				}
				else{
					$scope.userCookie = false;

					if($cookies.user == 'Reader'){ $scope.onlyReadCookie = true; }

				}

				$scope.userName;

				$scope.quitSpaces = function (){

					$scope.userName = $scope.userName.replace(/\s+/g, '');
				}				

			});


			app.controller('loginController' , function ($scope, $http, $cookies){

				

				$scope.userName;

				$scope.quitSpaces = function (){

					$scope.userName = $scope.userName.replace(/\s+/g, '');
				}				

			});


			app.controller('userSearchController' , function ($scope, $http, $cookies){

				if ($cookies.user == '010101super'){
					$scope.userCookie = true;	
				}
				else{
					$scope.userCookie = false;

					if($cookies.user == 'Reader'){ $scope.onlyReadCookie = true; }

				}


				$http.get('/user').

			    	success(function(data, status, headers, config) {
			    		
			    		$scope.users = data.results;
			    		
				    }).
				    error(function(data, status, headers, config) {
				      // log error
				    });

				 $scope.deleteUser = function (_id){

				 	$http.delete('/user/'+_id).

			    	success(function(data, status, headers, config) {
			    		
			    		setTimeout(function(){ window.location = '/deleted'; }, 30);
			    		
				    }).
				    error(function(data, status, headers, config) {
				      // log error
				    });

				 }


				 $scope.showDetails = function (_id){


					$http.get('/user/'+_id).

			    	success(function(data, status, headers, config) {
			    		
			    		$scope.userDetails = data;

			    		
				    }).
				    error(function(data, status, headers, config) {
				      // log error
				    });	
				 }


				
			});


			app.controller('confirmationController' , function ($scope, $http, $cookies){

				if ($cookies.user == '010101super'){
					$scope.userCookie = true;	
				}
				else{
					$scope.userCookie = false;

					if($cookies.user == 'Reader'){ $scope.onlyReadCookie = true; }

				}
	

			});

			app.controller('alertController' , function ($scope, $http, $cookies, $filter){

				if ($cookies.user == '010101super'){
					$scope.userCookie = true;	
				}
				else{
					$scope.userCookie = false;

					if($cookies.user == 'Reader'){ $scope.onlyReadCookie = true; }

				}

				var socket = io();
				socket.on('new visit', function(msg){

					$scope.visitAlert;
					var visitId = msg.visitId;

					$http.post('/people/id',{id : visitId}).

			    	success(function(data, status, headers, config) {
			    		
			    		$scope.brand = msg.brand;
			    		$scope.color = msg.color;
			    		$scope.placas = msg.placas;
			    		$scope.model = msg.model

			    		$scope.clusterId = msg.clusterId;
			    		$scope.visitAlert = data.name;
			    		
			    		

						$scope.date = $filter('date')(new Date(),'HH:mm:ss');
						

			    			$http.get('/cluster/'+$scope.clusterId).

					    	success(function(data, status, headers, config) {

					    		$scope.nickname = data.nickname;


					    		if ($scope.placas != undefined){

						    		var list = angular.element(document.querySelector('#list'));
									list.append('<a class="list-group-item" align="center"> Visita de:   <b>'+$scope.visitAlert+'</b> Con Automovil: <b>'+$scope.brand+' '+$scope.model+' '+$scope.color+'</b> Placas: <b>'+$scope.placas+'</b> Con destino a: <b>'+$scope.nickname+'</b><i>A las: <b>'+$scope.date+'</b> horas</i></a>')
								}
								else{

									var list = angular.element(document.querySelector('#list'));
									list.append('<a class="list-group-item" align="center"> Visita de:   <b>'+$scope.visitAlert+'</b><i> Sin Automovil Registrado </i><b></b> Con destino a: <b>'+$scope.nickname+'</b><i>A las: <b>'+$scope.date+'</b> horas</i></a>')

								}
						    })

			    		

			    		
				    }).
				    error(function(data, status, headers, config) {
				      // log error
				    })

				})

				$scope.flag = false;

				$scope.alertClick = function (){

					if ($scope.flag == false){
						$scope.flag == true;
					}

					if ($scope.flag == true) {$scope.flag = false;}

				}
	

			});

