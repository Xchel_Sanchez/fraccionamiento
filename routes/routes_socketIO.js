module.exports = (function (io){
	
	io.on('connection', function (socket){
  		 console.log('a user connected');

  		 socket.on('new visit', function(msg){
		    io.emit('new visit', msg);
		  });

  		 socket.on('disconnect', function(){
    		console.log('user disconnected');
		});
  	});	 
})