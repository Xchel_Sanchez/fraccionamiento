module.exports = (function (app){


	/* GET home page. */
	app.route('/')

	.get(function (req, res) {
	  res.render('login');
	})

	app.route('/confirmation')

	.get(function (req, res) {

		if (req.session._id) {
				res.render('confirmation',{Inserted: 'La Modificación'});
			}
			
			else {

	            req.session.destroy(function (err) {
	                res.redirect('/')
	            });
			}
	})

	app.route('/visitInserted')

	.get(function (req, res) {

		if (req.session._id) {
				res.render('confirmation',{Inserted: 'El Visitante'});
			}
			
			else {

	            req.session.destroy(function (err) {
	                res.redirect('/')
	            });
			}
	})


	app.route('/residentInserted')

	.get(function (req, res) {

		if (req.session._id) {
				res.render('confirmation',{Inserted: 'El Residente'});
			}
			
			else {

	            req.session.destroy(function (err) {
	                res.redirect('/')
	            });
			}
	})


	app.route('/deleted')

	.get(function (req, res) {

		if (req.session._id) {
				res.render('deleted');
			}
			
			else {

	            req.session.destroy(function (err) {
	                res.redirect('/')
	            });
			}
	})


	app.route('/start')

	.get(function (req,res){

		if (req.session._id) {
				res.render('index');
			}
			
			else {

	            req.session.destroy(function (err) {
	                res.redirect('/')
	            });
			}

		
	})

	app.route('/addresident')

	.get(function (req,res){

		if (req.session._id) {
				res.render('addResident');
			}
			
			else {

	            req.session.destroy(function (err) {
	                res.redirect('/')
	            });
			}

		
	})

	app.route('/addUser')

	.get(function (req,res){

		if (req.session._id && req.cookies.user == '010101super') {
				res.render('addUser');
			}
			
			else {

	            req.session.destroy(function (err) {
	                res.redirect('/')
	            });
			}

		
	})


	app.route('/userSearch')

	.get(function (req,res){

		if (req.session._id && req.cookies.user == '010101super') {
				res.render('userSearch');
			}
			
			else {

	            req.session.destroy(function (err) {
	                res.redirect('/')
	            });
			}

		
	})

	app.route('/addpeople')

	.get(function (req,res){

		if (req.session._id) {
				res.render('addPeople');
			}
			
			else {

	            req.session.destroy(function (err) {
	                res.redirect('/')
	            });
			}
		
	})

	app.route('/addVisit')
	.get(function (req,res){

		if (req.session._id) {
				res.render('addVisit');
			}
			
			else {

	            req.session.destroy(function (err) {
	                res.redirect('/')
	            });
			}
		
	})

	app.route('/residentSearch')
	.get(function (req,res){

		if (req.session._id) {
				res.render('residentFind');
			}
			
			else {

	            req.session.destroy(function (err) {
	                res.redirect('/')
	            });
			}
		
	})

	app.route('/peopleSearch')
	.get(function (req,res){

		if (req.session._id) {
				res.render('peopleSearch');
			}
			
			else {

	            req.session.destroy(function (err) {
	                res.redirect('/')
	            });
			}
		
	})

	app.route('/visitSearch')
	.get(function (req,res){

		if (req.session._id) {
				res.render('visitSearch');
			}
			
			else {

	            req.session.destroy(function (err) {
	                res.redirect('/')
	            });
			}
		
	})

	app.route('/residentProfile')
	.get(function (req,res){

		if (req.session._id) {
				res.render('residentProfile');
			}
			
			else {

	            req.session.destroy(function (err) {
	                res.redirect('/')
	            });
			}
		
	})

	app.route('/peopleProfile')
	.get(function (req,res){

		if (req.session._id) {
				res.render('peopleProfile');
			}
			
			else {

	            req.session.destroy(function (err) {
	                res.redirect('/')
	            });
			}
		
	})

	

	app.route('/addCluster')
	.get(function (req,res){

		if (req.session._id) {
				res.render('addCluster');
			}
			
			else {

	            req.session.destroy(function (err) {
	                res.redirect('/')
	            });
			}
		
	})

	app.route('/addHouse')
	.get(function (req,res){

		if (req.session._id) {
				res.render('addHouse');
			}
			
			else {

	            req.session.destroy(function (err) {
	                res.redirect('/')
	            });
			}
		
	})

	app.route('/example')
	.get(function (req,res){

				res.render('example');
			
	})

	app.route('/clusterSearch')
	.get(function (req,res){

		if (req.session._id) {
				res.render('clusterSearch');
			}
			
			else {

	            req.session.destroy(function (err) {
	                res.redirect('/')
	            });
			}		
		
	})

	app.route('/houseSearch')
	.get(function (req,res){

		if (req.session._id) {
				res.render('houseSearch');
			}
			
			else {

	            req.session.destroy(function (err) {
	                res.redirect('/')
	            });
			}		
		
	})

	app.route('/alertModule')
	.get(function (req,res){

		if (req.session._id) {
				res.render('alertModule');
			}
			
			else {

	            req.session.destroy(function (err) {
	                res.redirect('/')
	            });
			}		
		
	})

		
});


