var express = require('express');
var path = require('path');
var favicon = require('static-favicon');
var logger = require('morgan');
var bodyParser = require('body-parser');
var cookieParser 	= require('cookie-parser');
var session    		= require('express-session');
var multipart = require('connect-multiparty');


var app = express();
var http = require('http').Server(app);
var io = require('socket.io').listen(http);
var MongoClient = require('mongodb').MongoClient;
var Parse = require('node-parse-api').Parse;


var parseDB = new Parse("S7fpr3CsINC75UOsHqDUqXdj2I9oCnfW8i1AeLeF", "oqwmVdQMHBaDzjxOQca2TSQwomOhIvclg4MKB25K");




// view engine setup
app.set('views','./views');
app.set('view engine', 'jade');

app.use(favicon());
app.use(logger('dev'));
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));
app.use(multipart());
app.use(cookieParser());
app.use(session({

				secret: 'q~!!#s4HALA^MADRIDcds4<>>*S3--_-`´ç@',
				saveUninitialized: 	false,
				resave: 			false
			}));

app.use(express.static(__dirname+ '/public'));

//MongoClient.connect ("mongodb://localhost/FraccionamientoDB",function (err,FraccionamientoDB){
//if (err) throw err;

require('./routes/routes_www')(app);
require('./routes/routes_API')(app);
require('./routes/routes_socketIO')(io);



http.listen(3030,function () {
   
   console.log ('Escuchando por el puerto 3030');

});